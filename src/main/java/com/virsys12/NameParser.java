package main.java.com.virsys12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class NameParser {

    private final List<String> DICTIONARY_PREFIX = new ArrayList<String>() {{
        add("ms");
        add("miss");
        add("mrs");
        add("mr");
        add("master");
        add("rev");
        add("fr");
        add("dr");
        add("atty");
        add("prof");
        add("hon");
        add("pres");
        add("gov");
        add("coach");
        add("ofc");
        add("msgr");
        add("sr");
        add("br");
        add("supt");
        add("rep");
        add("sen");
        add("amb");
        add("treas");
        add("sec");
        add("pvt");
        add("cpl");
        add("sgt");
        add("adm");
        add("maj");
        add("capt");
        add("cmdr");
        add("lt");
        add("lt col");
        add("col");
        add("gen");
    }};

    private final List<String> DICTIONARY_SUR_AFFIX = new ArrayList<String>() {{
        add("a");
        add("ab");
        add("ap");
        add("abu");
        add("al");
        add("bar");
        add("bath");
        add("ben");
        add("bin");
        add("ibn");
        add("bet");
        add("bint");
        add("da");
        add("das");
        add("de");
        add("degli");
        add("dele");
        add("del");
        add("della");
        add("der");
        add("di");
        add("dos");
        add("du");
        add("e");
        add("el");
        add("fetch");
        add("fitz");
        add("i –");
        add("kil");
        add("la");
        add("le");
        add("m'");
        add("mac");
        add("mc");
        add("mhic");
        add("mic");
        add("mala");
        add("na");
        add("naka");
        add("neder");
        add("nic");
        add("ni");
        add("nin");
        add("nord");
        add("norr");
        add("ny");
        add("o");
        add("ua");
        add("ui'");
        add("öfver");
        add("ost");
        add("över");
        add("öz");
        add("pour");
        add("stor");
        add("söder");
        add("ter");
        add("ter");
        add("tre");
        add("van");
        add("väst");
        add("vest");
        add("von");
    }};

    private final List<String> DICTIONARY_SUFFIX = new ArrayList<String>() {{
        add("bvm");
        add("cfre");
        add("clu");
        add("cpa");
        add("csc");
        add("csj");
        add("dc");
        add("dd");
        add("dds");
        add("dmd");
        add("do");
        add("dvm");
        add("edd");
        add("esq");
        add("ii");
        add("iii");
        add("iv");
        add("inc");
        add("jd");
        add("jr");
        add("lld");
        add("ltd");
        add("md");
        add("od");
        add("osb");
        add("pc");
        add("pe");
        add("phd");
        add("ret");
        add("rgs");
        add("rn");
        add("rnc");
        add("shcj");
        add("sj");
        add("snjm");
        add("sr");
        add("ssmo");
        add("usa");
        add("usaf");
        add("usafr");
        add("usar");
        add("uscg");
        add("usmc");
        add("usmcr");
        add("usn");
        add("usnr");
    }};


    private static NameParser instance = null;

    protected NameParser() {
    }

    public static Name parseName(String fullName) {
        if (instance == null) {
            instance = new NameParser();
        }
        return instance.parseFullName(fullName);

    }

    private Name parseFullName(String fullName) {
        if (fullName == null) {
            return new Name(null, "", "", "", "", null);
        }
        fullName = fullName.trim();
        List<String> titles = parseTitles(fullName);
        List<String> suffixes = parseSuffixes(fullName);
        String firstName = parseFirstName(titles, fullName);
        String lastName = parseLastName(fullName, suffixes);
        String nickName = parseNickName(fullName);
        String middleName = parseMiddleName(fullName, titles, firstName, nickName, lastName, suffixes);
        if(firstName != null && firstName.equals(lastName)) {
            firstName = "";
        }

        return new Name(titles, firstName, nickName, middleName, lastName, suffixes);
    }

    private List<String> parseTitles(String input) {
        List<String> titles = new ArrayList<String>();
        String[] partitions = input.split(" ");
        for (String partition : partitions) {
            String strippedPartition = partition.replaceAll("[^A-Za-z]","");
            if (DICTIONARY_PREFIX.contains(strippedPartition.toLowerCase())) {
                titles.add(partition);
            } else {
                break; //We don't want affixes mixed in with titles, first instance of mismatch and we call it quits.
            }
        }
        return titles;
    }

    private String parseFirstName(List<String> titles, String input) {
        String titleString = titles.stream().collect(Collectors.joining(" "));
        String nameWithoutTitles = input;
        if (input.contains(titleString)) {
            nameWithoutTitles = input.substring(titleString.length());
        }
        String[] partitions = nameWithoutTitles.trim().split(" ");
        return partitions[0];
    }

    private List<String> parseSuffixes(String input) {
        List<String> suffixes = new ArrayList<String>();
        List<String> partitions = Arrays.asList(input.split(" "));
        Collections.reverse(partitions);
        for (String partition : partitions) {
            String strippedPartition = partition.replaceAll("[^A-Za-z]","");
            if (DICTIONARY_SUFFIX.contains(strippedPartition.toLowerCase())) {
                suffixes.add(partition);
            } else {
                break;
            }
        }
        Collections.reverse(suffixes);
        return suffixes;
    }

    private String parseLastName(String input, List<String> suffixes) {
        String suffixString = suffixes.stream().collect(Collectors.joining(" "));
        String nameWithoutSuffixes = input;

        if (input.contains(suffixString)) {
            nameWithoutSuffixes = input.substring(0, input.length() - suffixString.length());
        }
        String[] partitions = nameWithoutSuffixes.trim().split(" ");
        String lastName = partitions[partitions.length - 1];
        for (int i = partitions.length - 2; i > 0; i--) {
            String currentPartition = partitions[i];
            if (DICTIONARY_SUR_AFFIX.contains(currentPartition.toLowerCase())) {
                lastName = currentPartition + " " + lastName;
            } else {
                break;
            }
        }
        return lastName;
    }


    private String parseNickName(String input) {
        int doubleQuoteStart = input.indexOf("\"");
        int doubleQuoteEnd = input.lastIndexOf("\"");
        int singleQuoteStart = input.indexOf("'");
        int singleQuoteEnd = input.lastIndexOf("'");
        if (doubleQuoteStart != -1 && doubleQuoteEnd != -1) {
            return input.substring(doubleQuoteStart + 1, doubleQuoteEnd);
        }
        if (singleQuoteStart != -1 && singleQuoteEnd != -1 && singleQuoteStart != singleQuoteEnd) {
            return input.substring(singleQuoteStart + 1, singleQuoteEnd);
        }
        return "";
    }

    private String parseMiddleName(String fullName, List<String> titles, String firstName, String nickName, String lastName, List<String> suffixes) {
        String titleString = titles.stream().collect(Collectors.joining(" "));
        String suffixString = suffixes.stream().collect(Collectors.joining(" "));
        String noTitleName = titles.size() > 0 ? fullName.split(titleString)[1].trim() : fullName;

        String[] splitBySuffix = noTitleName.split(suffixString);
        String splicedBySuffix = splitBySuffix.length > 0 ? splitBySuffix[0].trim() : "";
        String noTitleSuffixName = suffixes.size() > 0 ? splicedBySuffix : noTitleName;

        String[] splitByFirstName = noTitleSuffixName.split(firstName);
        String splicedFirst = splitByFirstName.length > 1 ? splitByFirstName[1].trim() : "";
        String noTitleFirstSuffixName = !noTitleSuffixName.trim().isEmpty() ? splicedFirst : noTitleSuffixName;

        String[] splitByLastName = noTitleFirstSuffixName.split(lastName);
        String splicedLast = splitByLastName.length > 0 ? splitByLastName[0].trim() : "";
        String noTitleFirstLastSuffixName = !noTitleFirstSuffixName.trim().isEmpty() ? splicedLast : noTitleFirstSuffixName;


        String reversed = new StringBuilder(noTitleFirstLastSuffixName).reverse().toString();
        int nickStart = findIndexFirstSpaceAfterQuotes(reversed);
        int nickEnd = findIndexFirstSpaceAfterQuotes(noTitleFirstLastSuffixName);


        if (nickStart != -1) {
            return noTitleFirstLastSuffixName.substring(0, nickStart);
        }
        if (nickEnd != -1) {
            return noTitleFirstLastSuffixName.substring(nickEnd + 1);
        }

        return noTitleFirstLastSuffixName;
    }


    private int findIndexFirstSpaceAfterQuotes(String input) {
        boolean firstQuoteFound = false;
        char[] chars = input.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '\'' || chars[i] == '"') {
                if (firstQuoteFound) {
                    for (int j = i; j < chars.length; j++) {
                        if (chars[j] == ' ') {
                            return j;
                        }
                    }
                    return -1;
                }
                firstQuoteFound = true;
            }
        }
        return -1;
    }
}
