package main.java.com.virsys12;

import java.util.List;

public class Name {

    private List<String> titles;
    private String firstName;
    private String nickName;
    private String middleName;
    private String lastName;
    private List<String> suffixes;

    public Name(List<String> titles, String firstName, String nickName, String middleName, String lastName, List<String> suffixes) {
        this.titles = titles;
        this.firstName = firstName;
        this.nickName = nickName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.suffixes = suffixes;
    }

    public List<String> getTitles() {
        return titles;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getNickName() {
        return nickName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public List<String> getSuffixes() {
        return suffixes;
    }
}
