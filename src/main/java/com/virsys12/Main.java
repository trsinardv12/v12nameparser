package main.java.com.virsys12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        demo();

//        try {
//            File file = new File("C:\\Users\\Tim Sinard\\Desktop\\Example\\name_list.txt");
//            FileReader fileReader = new FileReader(file);
//            BufferedReader bufferedReader = new BufferedReader(fileReader);
//            StringBuffer stringBuffer = new StringBuffer();
//            String line;
//            while ((line = bufferedReader.readLine()) != null) {
//                Name name = NameParser.parseName(line);
//                System.out.println(name.getFirstName() + " " + name.getLastName());
//            }
//            fileReader.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
    }

    private static void demo() {
//        Name name = NameParser.parseName("Dr Mr Timothy ('Tim The Tank') Something Something Al Von Van Sinard III USMC");
//        Name name = NameParser.parseName("Dr. Thomas Jefferson Ph.D.");
//        Name name = NameParser.parseName("Kari Crawford");
        Name name = NameParser.parseName("Dr. Crawford");

        System.out.println("Titles: ");
        for (String s : name.getTitles()) {
            System.out.println("+ " + s);
        }
        System.out.println("First: " + name.getFirstName());
        System.out.println("Middle: " + name.getMiddleName());
        System.out.println("Nick: " + name.getNickName());
        System.out.println("Last: " + name.getLastName());
        System.out.println("Suffixes: ");
        for (String s : name.getSuffixes()) {
            System.out.println("+ " + s);
        }
    }
}
